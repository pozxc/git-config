# `PSReadLine`

https://github.com/lzybkr/PSReadLine

~~Bunu sanırım PowerShell'de üst ok/alt ok davranışını (geçmişte ilerleme) düzeltmek için kurdum.~~ Hayır PSReadLine modülü kendiliğinden kuruluyor ve üst ok/alt ok davranışını bozuyor.

## `PSReadLine` modülünü güncelleme:

```
powershell -noprofile -command "Update-Module PSReadline"
```

# log ve diff'lerde encoding sorunu

PowerShell'den veya cmd'den `locale` komutunu çalıştır. Eğer gösterilen sonuçta `LC_ALL` öğesinin değeri `C.UTF-8` görünüyorsa bir sorun olmamalı. Eğer değer boşsa key'i `LC_ALL`, değeri ise `C.UTF-8` olan bir environment variable ekle.

# diff-so-fancy

```
npm install -g https://github.com/monoblaine/diff-so-fancy.git#master
```

`.gitconfig` dosyasında zaten var ama, yine de burada bulunsun:

```
git config --global color.diff.old "white red bold"
git config --global color.diff.new "white green bold"
git config --global core.pager "diff-so-fancy | less --tabs=4 -RFX"
git config --global interactive.diffFilter "diff-so-fancy --patch-mode | less --tabs=4 -RFX"
```
